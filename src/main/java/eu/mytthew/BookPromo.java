package eu.mytthew;

public class BookPromo extends Book {
	Book book;
	double value = 0.2;

	public BookPromo(Book book) {
		super();
		this.book = book;
	}

	@Override
	public double getPrice() {
		double promoValue = book.price * value;
		return book.price - promoValue;
	}
}
