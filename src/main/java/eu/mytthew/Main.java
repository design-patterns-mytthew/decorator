package eu.mytthew;

public class Main {
	public static void main(String[] args) {
		Book book = new Book("Stephen King", "IT", 25.99);
		System.out.println(book.toString());
		BookPromo bookPromo = new BookPromo(book);
		SuperPromo superPromo = new SuperPromo(bookPromo);
		System.out.println(Math.round(bookPromo.getPrice() * 100.0) / 100.0);
		System.out.println(Math.round(superPromo.getPrice() * 100.0) / 100.0);
	}
}
