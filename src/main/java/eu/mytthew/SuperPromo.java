package eu.mytthew;

public class SuperPromo extends Book {
	Book book;
	double value = 0.3;

	public SuperPromo(Book book) {
		super();
		this.book = book;
	}

	@Override
	public double getPrice() {
		double promoValue = book.getPrice() * value;
		return book.getPrice() - promoValue;
	}
}
