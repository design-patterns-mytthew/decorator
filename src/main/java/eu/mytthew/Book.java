package eu.mytthew;

public class Book {
	String author;
	String title;
	double price;

	public Book(String author, String title, double price) {
		this.author = author;
		this.title = title;
		this.price = price;
	}

	public Book() {
	}

	public double getPrice() {
		return price;
	}

	@Override
	public String toString() {
		return "Author = '" + author + '\'' +
				"\nTitle = '" + title + '\'' +
				"\nPrice = " + getPrice();
	}
}
